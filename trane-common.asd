;;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciej@pasternacki.net>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:trane-common.system
  (:use #:common-lisp #:asdf))
(in-package #:trane-common.system)

(defsystem #:trane-common
  :name "Common parts of CL-Trane"
  :description "trane-common"
  :author "Maciej Pasternacki <maciej@pasternacki.net>"
  :licence "BSD sans advertising clause, see file COPYING for details"
  :components ((:module #:src :components ((:file "common"))))
  :depends-on (#:postmodern #:hunchentoot #:puri #:iterate #:py-configparser #:cl-base64 #:cl-qprint #:md5))

;;; Needs to be loaded AFTER cl-postgres and postmodern.  LAME!
(defmethod perform :after ((op load-op)
                           (system (eql (find-system :cl-postgres))))
  (operate 'load-op :simple-date))

(defsystem #:trane-common.test
  :description "Test suite for trane-common"
  :components ((:module #:t :components ((:file "common"))))
  :depends-on (#:trane-common #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :trane-common))))
  "Perform unit tests for trane-common"
  (asdf:operate 'asdf:load-op :trane-common.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam)) :trane-common))
