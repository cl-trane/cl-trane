(defpackage #:cl-trane-doc (:use #:common-lisp))
(in-package #:cl-trane-doc)

(defparameter *dir* (make-pathname :defaults *load-pathname*
                                   :name nil :type nil :version nil))
(asdf:oos 'asdf:load-op :cl-trane)
(load (merge-pathnames "dependency-graph.lisp" *dir*))
(load (merge-pathnames "lispdoc.lisp" *dir*)) ; http://homepage.mac.com/svc/openmcl/readme.html

(trane-dependency-graph:graph (merge-pathnames "dependency-graph.dot" *dir*))
(let ((trane-common:*config* (py-configparser:make-config))
      (trane-common:*db* nil)
      (trane-common:*handler-package* nil))
  (lispdoc:lispdoc-html *dir* :trane-common :trane-bb :trane-taxonomy :trane-passengers))
#+b0rken (tinaa:document-system ':asdf-system :cl-trane
                                (merge-pathnames "html/" *dir*))
